#!/bin/bash

# if you want to rebuild the conda environment, the easist thing to do is run this
#     docker stop dso_ex_conda && docker rm dso_ex_conda
# then run this script

set -ex

if ! docker exec dso_ex_conda python ; then
    docker run --tty --detach --volume $PWD:/home/app --name dso_ex_conda condaforge/miniforge3
    docker exec --workdir=/home/app dso_ex_conda conda env create -f environment.yml
    docker exec --workdir=/home/app dso_ex_conda sh utils/prep_pyppeteer.sh
fi

docker exec --workdir=/home/app dso_ex_conda /opt/conda/bin/conda run -n dso-ex python utils/spell_check.py
docker exec --workdir=/home/app dso_ex_conda /opt/conda/bin/conda run -n dso-ex python utils/md_to_html.py
docker exec --workdir=/home/app dso_ex_conda /opt/conda/bin/conda run -n dso-ex python utils/html_to_pdf.py
