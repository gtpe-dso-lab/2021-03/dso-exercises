#!/usr/bin/env python
# coding: utf-8

import asyncio
from pathlib import Path

from pyppeteer import launch

html_path = Path.cwd() / 'output' / 'html'

pdf_path = html_path.parent / 'pdf'
pdf_path.mkdir(exist_ok=True)

async def create_pdf(h, b):
    page=await b.newPage()

    print(f'Converting {h}')
    html_file_path = h.resolve()
    pdf_file_path = pdf_path / html_file_path.name.replace('.html', '.pdf')

    await page.goto(url=f'file:{html_file_path}', options={'waitUntil': ['load', 'domcontentloaded'],
                                                           'timeout': 300_000})
    await page.pdf({'path': pdf_file_path, 'printBackground': True,# 'preferCSSPageSize': True, 
                    'margin': {'top': '0.4in', 'right': '0.4in', 'bottom': '0.4in', 'left': '0.4in'}})
    print(f'Created {pdf_file_path}')

async def create_pdfs():
    browser=await launch(headless=True, args=['--no-sandbox', '--disable-dev-shm-usage'])

    tasks = []
    for h in html_path.glob('*.html'):
        tasks.append(create_pdf(h, browser))

    await asyncio.gather(*tasks)
    await browser.close()

# for reasons that are unclear, this only works in CI if debug=True
asyncio.run(create_pdfs(), debug=True)
