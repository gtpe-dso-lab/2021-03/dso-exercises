import argparse
from pathlib import Path

parser = argparse.ArgumentParser()
parser.add_argument('Path',
                    metavar='path',
                    type=str,
                    help='the path to file to sort')

args = parser.parse_args()

file_path = args.Path

if not Path(file_path).is_file():
    print('The path specified does not exist')
    sys.exit()

sorted_words = sorted(list(set(Path(file_path).read_text().split())), key=lambda v: (v.casefold(), v))

Path(file_path).write_text('\n'.join(sorted_words))