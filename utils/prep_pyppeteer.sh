#!/bin/bash

conda run -n dso pyppeteer-install
apt-get update
DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends libx11-xcb1 libxcomposite1 libxcursor1 \
                                           libxdamage1 libxext6 libxfixes3 libxi6 libxrender1 libxtst6 libglib2.0-0 \
                                           libnss3 libnspr4 libcups2 libdbus-1-3 libxss1 libxrandr2 libasound2 \
                                           libpangocairo-1.0-0 libpango-1.0-0 libcairo2 libatk1.0-0 libgtk-3-0
dpkg --configure -a
